all : socket.o socket_server.o socket_cliente.o server client

CPPFLAGS = -g -I.

server : server.c
	cc -g -I. socket.o socket_server.o server.c -o server

client : cliente.c
	cc -g -I. socket.o socket_cliente.o cliente.c -o client

clean :
	rm *.o client server
